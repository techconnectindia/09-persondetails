# Iterating an array of Person objects
* **Objective:**
    * To concatenate the details of `Person` objects by iterating an array  

* **Purpose:**
    * To demonstrate practical understanding of `while`, `for`, and `for each` loops.

* **Getting started:**
    * The [PersonHandler](src/main/java/com/jpmchase/techconnect/PersonHandler.java) class has 3 [method stubs](https://en.wikipedia.org/wiki/Method_stub) to be completed.
        * The methods contain line-by-line comments to guide you through the process.
    * Ensure all tests cases pass successfully.
